ThinkPHP 6.0 基于swoole websocketio聊天室
===============
asdf阿斯顿发送到发斯蒂芬
> 运行环境要求PHP7.1+ 及 swoole

## 主要新特性

* 感谢插件提供者
* 前端页面 [litewebchat_frame](https://gitee.com/mirrors/litewebchat_frame)
* Identicon 自动生成像素头像
* 目前页面有些小bug，不影响使用

## 部署

~~~
git clone https://gitee.com/Jooeys/liaotianshi.git
~~~

切换到项目中启动swoole
~~~
php think swoole start
~~~

![微信截图_20200525144702.png](http://markdown-1252796956.cos.ap-beijing.myqcloud.com/2020/05/25/ad1a08b99eb9c5b9021117b70db73146.png)
![微信截图_20200525144713.png](http://markdown-1252796956.cos.ap-beijing.myqcloud.com/2020/05/25/a027a17707b523384da43f6638655f2b.png)
![微信截图_20200525144725.png](http://markdown-1252796956.cos.ap-beijing.myqcloud.com/2020/05/25/4e10d1b453465506d2b822650e1099db.png)
